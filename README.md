# TranscribeIt - Projeto para Deficientes Auditivos

## Este projeto possui 2 módulos:
* nodeSpeech - Client feito em Node que faz a requisição para para o google speech e grava as informações no firebase e **não funciona no windows ainda**
* webSpeech - Código Javascript e HTML do site https://recognize-97a48.appspot.com/


## Para usar a aplicação no seu projeto siga os passos:
1. Peça a keycode por HO para flisbao
2. Instalar o Git-Crypt:
    ```
    apt install git-crypt #linux
    ```
    
    ```
    brew install git-crypt #mac os
    ```

3. Execute os comandos
    ```
    apt-get install haveged rng-tools
    ```
    
    ```
    gpg --gen-key
    ```

4. Execute os passos
    * Enter para selecionar a opção default
    * Em keysize coloque 4096
    * Enter para selecionar a opção default
    * y para expiração
    * Digite seu nome usado no Github
    * Digite seu email usado no Github
    * Crie uma senha
5. Adicione o seu usuário para o projeto:
    ```
    git-crypt add-gpg-user SEU_EMAIL_GPG
    ```
6. Com o arquivo chave em mãos vá para a pasta raiz do projeto e digite:
    ```
    git-crypt unlock MINHA_CHAVE
    ```
7. Caso não tenha o node instalado siga o tutorial abaixo:
    * Linux: https://www.digitalocean.com/community/tutorials/how-to-install-node-js-on-ubuntu-16-04
    * Mac: http://nodesource.com/blog/installing-nodejs-tutorial-mac-os-x/

8. Instalar as dependências
    ```
    npm install
    ```
    
    ```
    brew install sox #mac os
    ```

9. Caso esteja rodando no Mac, na linha 112, alterar "arecord" para "sox"

### Ufa, Agora é pra funcionar rs
### Voltando ao Node, voltar à pasta do node 
``` 
cd nodeSpeech
node app.js NOME-DO-CANAL
```
### E **Voilá**, só ligar o microfone no computador e sair falando.
#### À fim de que consiga ver no site a transcrição do áudio, deve-se fazer o login com o usuário e senha informado à você após o contato para pegar a chave no site: https://recognize-97a48.appspot.com e após o login, digitar o nome do canal e o sistema estará funcionando.



## WebSpeech
### O WebSpeech é o projeto que está hospedado no AppEngine do Google e que se conecta com o firebase.
### Ele é uma aplicação feita com vanilla JavaScript que se conecta com o firebase e mostra a legenda.
### Caso queira rodar na sua máquina local siga o tutorial: https://cloud.google.com/appengine/docs/standard/python/tools/using-local-server
### Caso queira contribuir com uma melhoria ou adicionar uma nova feature pode sempre realizar um pull request.

#### Qualquer dúvida ou sugestão por favor me procurem por HO (flisbao) e "Happy Transcribing".



