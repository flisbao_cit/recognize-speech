/**
 * Get argument
 */

const args = process.argv.slice(2);

const channel = args[0];
const user = args[1];

if (!channel) {
    console.log('Required arguments not found, please run: node app.js channel email password');
    return false;
}


/**
 * Initialize all modules
 */

const Record = require('node-record-lpcm16');
const Speech = require('@google-cloud/speech');
const Firebase = require('firebase-admin');


/**
 * Setup firebase
 */
const serviceAccount = require('./keyfile.json');

Firebase.initializeApp({
    credential: Firebase.credential.cert(serviceAccount),
    databaseURL: 'https://recognize-97a48.firebaseio.com'
});

/**
 * Setup google speech
 */
const recognizeSpeech = new Speech.SpeechClient({
    projectId: 'recognize-97a48',
    keyFilename: 'keyfile.json'
});

const requestSpeech = {
    config: {
        encoding: 'LINEAR16',
        sampleRateHertz: 16000,
        languageCode: 'pt_BR',
        profanityFilter: true
    },
    interimResults: true,
    singleUtterance: true
};


/**
 * Function start
 * It will be enabled several times
 */

const recordStart = () => {

    let stopped = false;

    const stop = () => {

        if (stopped) {
            return;
        }

        stopped = true;
        Record.stop();
        loop();
    };

    /**
     * Make request stream to google speech
     * 
     * and on each event make an action
     */
    const recognizeStream = recognizeSpeech.streamingRecognize(requestSpeech)
        .on('error', console.error)
        .on('data', (data) => {
            const result = data.results[0];

            if (!result || !result.alternatives || result.alternatives.length === 0) {
                stop();
                return;
            }

            //If there is any alternative results we are going to receive it
            const transcriptResult = result.alternatives[0];

            console.log(`final=${result.isFinal}, transcription: ${transcriptResult.transcript}`);

            //send to firebase
            sendToFirebase(transcriptResult.transcript, result.isFinal);

            if (result.isFinal) {
                stop();
            }
        })
        .on('end', () => {
            stop();
        });

    //Start record

    Record.start({
        sampleRateHertz: 16000,
        threshold: 0,
        // Other options, see https://www.npmjs.com/package/node-record-lpcm16#options
        // verbose: true,
        recordProgram: 'arecord', // Try also "arecord" or "sox"
        silence: '0:00.500',
    })
        .on('error', console.error)
        .pipe(recognizeStream);

    console.log('Listening, press Ctrl+C to stop.');
};

//Send to a unique channel on firebase
const sendToFirebase = (trancriptText, isFinal) => {
    if (!channel || channel === undefined) {
        console.error('Should pass an channel argument in order to run!!!');
        return;
    }
    const ref = Firebase.database().ref(channel).push();
    ref.set({
        transcript: trancriptText,
        isFinal: isFinal,
        user: user ? user : null
    });
};

const restart = () => {
    Record.stop();
    loop();
};

const loop = () => {
    recordStart();
};

loop();





