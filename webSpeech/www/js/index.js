var channel = null;


var config = {
    apiKey: "AIzaSyA_NGpcJ8eY4Q9RHZ76nkMyKoHgHsfpHqg",
    authDomain: "recognize-97a48.firebaseapp.com",
    databaseURL: "https://recognize-97a48.firebaseio.com",
    projectId: "recognize-97a48",
    storageBucket: "recognize-97a48.appspot.com",
    messagingSenderId: "960160428953"
};

var init = false;
var toggle = false;
var defaultFontSize = 20;
var app = firebase.initializeApp(config);


function startListening(channel) {
    var big = document.getElementById('text');
    var subtitle = document.getElementById('subtitle');
    subtitle.style.fontSize = defaultFontSize + 'px';
    var events = firebase.database().ref(channel);

    events.on('child_added', function (snap) {
        if (init) {
            var text = '';
            if (toggle) {
                snap.val().isFinal ? big.innerHTML += '<br/> ' + snap.val().transcript : '';
                autoScroll(big);
            }
            if (snap.val()) {
                if (snap.val().user) {
                    var user = snap.val().user;
                    text = user + ' disse: '+ snap.val().transcript;
                } else {
                    text = snap.val().transcript;
                }
            }
            subtitle.innerHTML = text;
        }
    });

    events.once('value', function () {
        init = true;
    });
    
}

function autoScroll(big) {
    setTimeout(function () {
        if (big.scrollHeight - big.scrollTop !== big.clientHeight) {
            big.scrollTop += 50;
        }
    }, 1000);
}

function btnTransmitChannelClicked(event) {
    event.preventDefault();
    document.querySelector('[data-channel-transmit]').classList.remove('hidden');
    btnListenChannelClicked(event);
}

function btnListenChannelClicked(event) {
    event.preventDefault();
    if (!channel) {
        channel = document.querySelector('[data-listen-channel-value]').value;
    }
    if (channel.length >= 3) {
        document.querySelector('[data-channel]').classList.remove('hidden');
        document.querySelector('[data-welcome]').classList.add('hidden');
        startListening(channel);
    }
}

function doBack(event) {
    event.preventDefault();
    window.location.reload();
}

function signIn(event) {
    event.preventDefault();

    var email = document.getElementById('username').value;
    var password = document.getElementById('password').value;
    var login = true;
    firebase.auth().signInWithEmailAndPassword(email, password)
        .then(function () {
            var signIn = document.querySelector('.signIn');
            var container = document.querySelector('[data-app]');
            signIn.classList.add('hidden');
            container.classList.remove('hidden');
        })
        .catch(function (err) {
            console.log(err);
        });
}


function start() {

    firebase.auth().signOut()
        .catch(function (err) {
            console.error(err);
        });

    var btnListenChannel = document.querySelector('[data-listen-channel]');
    var btnTransmitChannel = document.querySelector('[data-transmit-channel');
    var btnBack = document.querySelector('[data-listen-back]');
    if (btnListenChannel && btnTransmitChannel) {
        btnListenChannel.addEventListener('click', btnListenChannelClicked);
        btnTransmitChannel.addEventListener('click', btnTransmitChannelClicked);
    }

    if (btnBack) {
        btnBack.addEventListener('click', doBack);
    }

    var signInButton = document.querySelector('[data-login-app]');
    if (signInButton) {
        signInButton.addEventListener('click', signIn);
    }

    document.getElementById('switch-sm').addEventListener('change', function () {
        toggle = !toggle;
    });

    document.getElementById('up').addEventListener('click', function () {
        defaultFontSize++;
        subtitle.style.fontSize = defaultFontSize + 'px';
    });

    document.getElementById('down').addEventListener('click', function () {
        defaultFontSize--;
        subtitle.style.fontSize = defaultFontSize + 'px';
    });
}

start();
